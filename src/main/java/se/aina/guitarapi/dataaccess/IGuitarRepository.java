package se.aina.guitarapi.dataaccess;

import se.aina.guitarapi.models.domain.Guitar;
import se.aina.guitarapi.models.dto.GuitarBrandDto;
import se.aina.guitarapi.models.dto.GuitarShortDTO;

import java.util.HashMap;

public interface IGuitarRepository {
    public HashMap<Integer, Guitar> getAllGuitars();
    public Guitar getGuitar(int id);
    public Boolean checkValidId(int id);
    public Boolean checkValid(Guitar guitar);
    public Boolean checkValid (Guitar guitar, int id);
    public Boolean existCheck(Guitar guitar);
    public void addGuitar(Guitar guitar);
    public void removeGuitar(int id);
    public GuitarShortDTO getGuitarShort(int id);
    public GuitarBrandDto getBrands();
    public void replaceGuitar(Guitar guitar);
    public void modifyGuitar(Guitar guitar);
   // public void removeGuitar(Guitar guitar);

}
