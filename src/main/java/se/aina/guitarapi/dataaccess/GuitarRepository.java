package se.aina.guitarapi.dataaccess;

import org.springframework.stereotype.Component;
import se.aina.guitarapi.models.domain.Guitar;
import se.aina.guitarapi.models.dto.GuitarBrandDto;
import se.aina.guitarapi.models.dto.GuitarShortDTO;
import se.aina.guitarapi.models.maps.GuitarBrandDtoMapper;
import se.aina.guitarapi.models.maps.GuitarDtoMapper;

import java.util.HashMap;

import static se.aina.guitarapi.models.maps.GuitarBrandDtoMapper.mapBrandDto;

@Component
public class GuitarRepository implements IGuitarRepository {
    private HashMap<Integer, Guitar> guitars = seedGuitars();

    private HashMap<Integer, Guitar> seedGuitars(){
        var guitars = new HashMap<Integer, Guitar>();
        guitars.put(1, new Guitar(1,"Telecaster","Jazzmaster"));
        guitars.put(2, new Guitar(2, "DanElectric","Guitarthingie"));
        return guitars;
    }

    public HashMap<Integer, Guitar> getAllGuitars(){
        return guitars;
    }

    public Guitar getGuitar(int id){
        return guitars.get(id);
    }
///
    @Override
    public Boolean checkValidId(int id) {
        return guitars.containsKey(id);
    }

    @Override
    public Boolean checkValid(Guitar guitar) {
        return (guitar.getId() > 0 && guitar.getBrand() != null && guitar.getModel() != null);
    }

    //Used when modifying or replacing guitar.
    @Override
    public Boolean checkValid(Guitar guitar, int id){
        return (id == guitar.getId() && checkValid(guitar));
    }


    @Override
    public Boolean existCheck(Guitar guitar) {
        return guitars.containsKey(guitar.getId());
    }

    @Override
    public void addGuitar(Guitar guitar) {

        guitars.put(guitar.getId(), guitar);
    }



    @Override
    public void removeGuitar(int id) {
        guitars.remove(id);
    }

    @Override
    public GuitarShortDTO getGuitarShort(int id) {
        var guitar = getGuitar(id);
        return  GuitarDtoMapper.mapGuitarShortDto(guitar);
    }

    @Override
    public GuitarBrandDto getBrands() {
        return mapBrandDto(guitars);
    }

    @Override
    public void replaceGuitar(Guitar guitar) {
        guitars.remove(guitar.getId());
        guitars.put(guitar.getId(), guitar);
    }

    @Override
    public void modifyGuitar(Guitar guitar) {
        var guitarToModify = getGuitar(guitar.getId());
        guitarToModify.setBrand(guitar.getBrand());
        guitarToModify.setModel(guitar.getModel());
    }
}
