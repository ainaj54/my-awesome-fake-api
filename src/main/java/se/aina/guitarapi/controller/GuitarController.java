package se.aina.guitarapi.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.aina.guitarapi.dataaccess.GuitarRepository;
import se.aina.guitarapi.dataaccess.IGuitarRepository;
import se.aina.guitarapi.models.domain.Guitar;
import se.aina.guitarapi.models.dto.GuitarBrandDto;
import se.aina.guitarapi.models.dto.GuitarShortDTO;

import java.util.HashMap;

@RestController
@RequestMapping(value= "/api/guitars/")
public class GuitarController {

    IGuitarRepository guitarRepository;

    @Autowired
    public GuitarController(GuitarRepository guitarRepository){
    this.guitarRepository = guitarRepository;
    }

    @GetMapping()
    public HashMap<Integer, Guitar> getAllGuitars() {

        return guitarRepository.getAllGuitars();
    }

    @GetMapping("/{id}/short")
    public ResponseEntity<GuitarShortDTO> getShortGuitar(@PathVariable int id){
        if(!guitarRepository.checkValidId(id)){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<GuitarShortDTO>(guitarRepository.getGuitarShort(id), HttpStatus.OK);
    }


    @GetMapping("/{id}")
    public ResponseEntity<Guitar> getGuitar(@PathVariable int id){
        if (guitarRepository.checkValidId(id)){
            Guitar guitar = guitarRepository.getGuitar(id);
            return new ResponseEntity<Guitar>(guitar, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/brands")
    public ResponseEntity<GuitarBrandDto> getGuitarBrands(){
        return new ResponseEntity<>(guitarRepository.getBrands(),HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> removeGuitar(@PathVariable int id){
        if(!guitarRepository.checkValidId(id)){
            return new ResponseEntity<>("input not found in repository", HttpStatus.BAD_REQUEST);
        }
        guitarRepository.removeGuitar(id);
        return new ResponseEntity<>("Guitar has been removed", HttpStatus.OK);
    }

    @DeleteMapping()
    public ResponseEntity removeGuitar(@RequestBody Guitar guitar){
        if(!guitarRepository.checkValid(guitar)){
            return new ResponseEntity<>("input not in correct format",HttpStatus.BAD_REQUEST);
        }
        if(!guitarRepository.existCheck(guitar)){
            return new ResponseEntity<>("Does not exist in repository", HttpStatus.BAD_REQUEST);
        }
        guitarRepository.removeGuitar(guitar.getId());
        return new ResponseEntity<>("Guitar has been removed", HttpStatus.OK);
    }


    @PostMapping()
    public ResponseEntity<Object> addGuitar(@RequestBody Guitar guitar){
        if(!guitarRepository.checkValid(guitar)){
            return new ResponseEntity<>("body not in correct format.", HttpStatus.BAD_REQUEST);
        }

        if(guitarRepository.existCheck(guitar)){
            return new ResponseEntity<>("Guitar already exist.", HttpStatus.BAD_REQUEST);
        }

        guitarRepository.addGuitar(guitar);
        return new ResponseEntity<>("Guitar has been added to repository", HttpStatus.CREATED);

    }

    @PutMapping("{id}")
    public ResponseEntity<Guitar> replaceGuitar(@PathVariable int id, @RequestBody Guitar guitar) {
        if (!guitarRepository.checkValid(guitar, id)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        if(!guitarRepository.existCheck(guitar)){
            addGuitar(guitar);
        }
        guitarRepository.replaceGuitar(guitar);
        return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);
    }

    @PatchMapping("{id}")
    public ResponseEntity<Guitar> modifyGuitar(@PathVariable int id, @RequestBody Guitar guitar){
        if (!guitarRepository.checkValid(guitar, id)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        if(!guitarRepository.existCheck(guitar)){

            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        guitarRepository.modifyGuitar(guitar);
        return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);
    }

}
