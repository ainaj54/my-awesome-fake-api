package se.aina.guitarapi.models.dto;

public class GuitarShortDTO {
    private String guitarModel;

    public GuitarShortDTO(String guitarModel) {
        this.guitarModel = guitarModel;
    }

    public String getGuitarModel() {
        return guitarModel;
    }

    public void setGuitarModel(String guitarModel) {
        this.guitarModel = guitarModel;
    }
}
