package se.aina.guitarapi.models.dto;

import java.util.HashMap;

public class GuitarBrandDto {
    private HashMap<String, Integer> guitarBrandsSummary;

    public GuitarBrandDto(HashMap<String, Integer> guitarBrandsSummary) {
        this.guitarBrandsSummary = guitarBrandsSummary;
    }

    public HashMap<String, Integer> getGuitarBrandsSummary() {
        return guitarBrandsSummary;
    }

    public void setGuitarBrandsSummary(HashMap<String, Integer> guitarBrandsSummary) {
        this.guitarBrandsSummary = guitarBrandsSummary;
    }
}
