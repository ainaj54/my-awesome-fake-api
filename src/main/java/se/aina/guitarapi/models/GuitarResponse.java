package se.aina.guitarapi.models;

import org.springframework.stereotype.Component;
import se.aina.guitarapi.models.domain.Guitar;

@Component
public class GuitarResponse {
    private Guitar guitar;
    private String message;

    public GuitarResponse(Guitar guitar, String message) {
        this.guitar = guitar;
        this.message = message;
    }

    public GuitarResponse() {
    }

    public Guitar getGuitar() {
        return guitar;
    }

    public void setGuitar(Guitar guitar) {
        this.guitar = guitar;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
