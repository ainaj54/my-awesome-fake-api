package se.aina.guitarapi.models.maps;

import se.aina.guitarapi.models.domain.Guitar;
import se.aina.guitarapi.models.dto.GuitarShortDTO;

import java.util.Locale;

public class GuitarDtoMapper {
    public static GuitarShortDTO mapGuitarShortDto(Guitar guitar){
        var shortGuitar = new GuitarShortDTO(guitar.getModel().toUpperCase());
        return shortGuitar;
    }
}
