package se.aina.guitarapi.models.maps;

import se.aina.guitarapi.dataaccess.GuitarRepository;
import se.aina.guitarapi.models.domain.Guitar;
import se.aina.guitarapi.models.dto.GuitarBrandDto;

import java.util.HashMap;

public class GuitarBrandDtoMapper {

    public static GuitarBrandDto mapBrandDto(HashMap<Integer,Guitar> guitars){
        HashMap<String,Integer> brandSummary = new HashMap<>();
        String brand;
        for(Integer key : guitars.keySet()){
            brand = guitars.get(key).getBrand();
            if(brandSummary.get(brand) == null){
                brandSummary.put(brand, 1);
            }else{
                var value = brandSummary.get(brand);
                brandSummary.put(brand, value+1);
            }
        }

        return new GuitarBrandDto(brandSummary);

    }

}
